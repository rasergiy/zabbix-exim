#!/usr/bin/env python3.4

'''
Exim mail server monitoring module.
Tested on FreeBSD 10.2, Python 2.7, 3.4
Author: Sergei Raskin
E-mail: rasergiy@gmail.com
'''

import re
import os
import sys
import json
import time
import codecs
import socket
import logging
import tempfile
import optparse
import subprocess


hostname         = 'mail'       # Zabbix's hostname (-n)
zabbix_confd     = "/usr/local/etc/zabbix24/zabbix_agentd.conf" # Zabbix config (-c)
eximlog          = '/var/log/exim/mainlog'  # Exim log (-e)

eximlog_archived = '%(eximlog)s.%(daysago)d'
eximqueue        = 'exim -bpc'
eximstat         = 'eximstats -nvr %(logs)s'
zabbix_sender    = "zabbix_sender"
clamav           = 'clamscan -V'
clamav_tfmt      = '%a %b %d %H:%M:%S %Y'

check_processes  = (
        ('exim', '/var/run/exim.pid'),
        ('spamd', '/var/run/spamd/spamd.pid'),
        ('clamav',  '/var/run/clamav/clamd.pid'),
        ('freshclam', '/var/run/clamav/freshclam.pid'))

log  = logging.getLogger('zabbix')
log.setLevel(logging.DEBUG)
log.handlers=[]

parser = optparse.OptionParser('%prog [lld/stat]')
parser.add_option("-l", "--logs-count", dest="lcount",
    default=1, help='Log files count')
parser.add_option("-c", "--zabbix-conf", dest="zabbix_agentd_config", 
    default=zabbix_confd, help='Zabbix agentd config')
parser.add_option("-e", "--eximlog", dest="eximlog", 
    default=eximlog, help='Exim log path')
parser.add_option("-n", "--hostname", dest="hostname", 
    default=hostname, help='Hostname')
parser.add_option("-a", "--eximstat", dest="eximstat", action='store_true',
    default=False, help='Eximstat statistic')
parser.add_option("-f", "--freshclam", dest="freshclam", action='store_true',
    default=False, help='Freshclam information')
parser.add_option("-p", "--processes", dest="processes", action='store_true',
    default=False, help='Processes information')
parser.add_option("-t", "--leave-temp", dest="leavetemp", action='store_true',
    default=False, help='Leave temp')
parser.add_option("-s", "--no-send", dest="nosend", action='store_true',
    default=False, help='Do not send to zabbix')
parser.add_option("-v", "--verbose", dest="verbose", action='store_true',
    default=False, help='Verbose output')
options, args = parser.parse_args()

cons=logging.StreamHandler(sys.stdout) 
cerr=logging.StreamHandler(sys.stderr) 
cerr.setLevel(logging.ERROR)
if options.verbose:
    cons.setLevel(logging.DEBUG)
else:
    cons.setLevel(logging.WARNING)
log.addHandler(cons)
log.addHandler(cerr)


def is_running(pid_file):
    ''' If sig is 0, then no signal is sent, but error checking is 
    still performed; this can be used to check for the existence of 
    a process ID or process group ID.'''
    try:
        with open(pid_file, 'rt') as f:
            pid = f.read().strip()
    except FileNotFoundError:
        return 0
    try:
        os.kill(int(pid), 0)
        return 1
    except ProcessLookupError:
        return 0

def run(cmd, fail=True):
    try: 
        log.debug('RUN: %s' % cmd)
        ret = subprocess.check_output(cmd, shell=True, 
            stderr=subprocess.STDOUT)
        ret = ret.decode('utf-8', errors="ignore").strip()
        log.debug('Received output (%s bytes): %s ...' % (len(ret), ret[:10]))
        return ret
    except subprocess.CalledProcessError as err: 
        out = err.output.decode('UTF-8') 
        msg = 'Command failed: %s\n%s' % (cmd, out)
        if fail:
            log.error(msg)
            raise err
        log.warn(msg)
        return None

def dumpkeys(keys):
    return '\n'.join('%s %s %s' % (options.hostname, k, keys[k]) for k in sorted(keys))

def send(text):
    log.debug('- SEND %s' % ('-'*80))
    log.debug(text)
    f = tempfile.NamedTemporaryFile(delete=False)
    log.debug('Temp file: %s' % f.name)
    f.write(text.encode())
    f.close()
    run('%s -c %s -i %s' % (zabbix_sender, options.zabbix_agentd_config, f.name), False)
    if options.leavetemp:
        log.info('Temp file is not deleted: %s' % f.name)
    else:
        os.remove(f.name)

def push(keys, regex, output, *args):
    m = re.search(regex, output, re.MULTILINE)
    if m:
        for key, group in args:
            keys[key] = m.group(group)

# -----------------------------------------------------------------------------
# Gather output 
# -----------------------------------------------------------------------------


retcode = 0
keys = dict()


if options.freshclam:
    clam_version, clam_db_version, clam_db_timestamp_str = run(clamav).split('/')
    clam_db_timestamp = time.mktime(time.strptime(clam_db_timestamp_str, clamav_tfmt))
    keys['exim.clamav.database.timestamp']=clam_db_timestamp
    keys['exim.clamav.database.daysold']=(time.time()-clam_db_timestamp)/60/60/24
    keys['exim.clamav.database.version']=clam_db_version

if options.processes:
    for proc in check_processes:
        keys['exim.process.%s' % proc[0]]=is_running(proc[1])
    push(keys, '.*', run(eximqueue),
            ('exim.queue.count', 0),
            ('exim.queue.speed', 0))

if options.eximstat:
    nlogname = lambda n: eximlog_archived % {
                'eximlog': options.eximlog,
                'daysago': n }
    eximstat_logs = [options.eximlog,] + list(nlogname(i) \
                for i in range(int(options.lcount)-1))
    eximstat_out = run(eximstat % {'logs': ' '.join(eximstat_logs)}, False)
    if eximstat_out:
        push(keys, 'Received\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+([0-9.]+)%\s+(\d+)\s+([0-9.]+)%', eximstat_out,
                ('exim.received.bytes', 1),
                ('exim.received.bytes.speed', 1),
                ('exim.received.speed', 2),
                ('exim.received.count', 2),
                ('exim.received.hosts', 3),
                ('exim.delayed.count', 4),
                ('exim.delayed.percent', 5),
                ('exim.failed.count', 6),
                ('exim.failed.percent', 7))
        push(keys, 'Delivered\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)', eximstat_out,
                ('exim.delivered.bytes', 1),
                ('exim.delivered.bytes.speed', 1),
                ('exim.delivered.count', 2),
                ('exim.delivered.speed', 2),
                ('exim.delivered.addresses', 4),
                ('exim.delivered.hosts', 4))
        push(keys, 'Rejects\s+(\d+)\s+(\d+)', eximstat_out,
                ('exim.rejects.hosts', 2),
                ('exim.rejects.count', 1),
                ('exim.rejects.speed', 1))
        push(keys, 'Temp Rejects\s+(\d+)\s+(\d+)', eximstat_out, 
                ('exim.temprejects.hosts', 2),
                ('exim.temprejects.count', 1),
                ('exim.temprejects.speed', 1))
        push(keys, 'Ham\s+(\d+)\s+(\d+)', eximstat_out,
                ('exim.ham.hosts', 2),
                ('exim.ham.count', 1),
                ('exim.ham.speed', 1))
        push(keys, 'Spam\s+(\d+)\s+(\d+)', eximstat_out, 
                ('exim.spam.hosts', 2),
                ('exim.spam.count', 1),
                ('exim.spam.speed', 1))
        push(keys, 'Errors encountered:\s+(\d+)', eximstat_out, 
                ('exim.errors.count', 1),
                ('exim.errors.speed', 1))
    else:
        log.warn('No output received')
        retocde = -1

if keys:
    keystr = dumpkeys(keys)
    if options.nosend:
        print(keystr)
    else:
        send(keystr)
sys.exit(retcode)
